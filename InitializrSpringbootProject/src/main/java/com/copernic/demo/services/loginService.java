/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.demo.services;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

/**
 *
 * @author Ruben
 */
@Service
public class loginService {

    public int intents = 3;
    public String sitio = "login";

    public String comprovaDades(String nombre, String contrasenya, Model model) {
        String destino = null;
        if (nombre.equals("Marcelo")) {
            model.addAttribute("nombre", nombre);
            if (contrasenya.equals("123")) {
                model.addAttribute("contrasenya", contrasenya);
                destino = "accedido";
                intents = 3;
                sitio = "login";
            } else if (contrasenya.isBlank()) {
                destino = sitio;
            } else if (!contrasenya.equals("123")) {
                intents--;
                switch (intents) {
                    case 2:
                        destino = "intento2";
                        sitio = destino;
                        break;
                    case 1:
                        destino = "intento1";
                        sitio = destino;
                        break;
                    case 0:
                        destino = "error";
                        intents = 3;
                        sitio = "login";
                        break;
                    default:
                        break;
                }

            }
        } else if (nombre.isBlank()) {
            destino = sitio;
        } else if (!nombre.equals("Marcelo")) {
            intents--;
            switch (intents) {
                case 2:
                    destino = "intento2";
                    sitio = destino;
                    break;
                case 1:
                    destino = "intento1";
                    sitio = destino;
                    break;
                case 0:
                    destino = "error";
                    intents = 3;
                    sitio = "login";
                    break;
                default:
                    break;
            }
        }
        return destino;
    }
}

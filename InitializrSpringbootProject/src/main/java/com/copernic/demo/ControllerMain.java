/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.demo;


import com.copernic.demo.services.loginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Ruben
 */
@Controller
@Slf4j
public class ControllerMain {
    
    @Autowired
    private loginService loginService;
    
    @GetMapping("/login")
    public String login(){
        return "login";
    }
    
    @PostMapping("/login")
    public String signUp(
            @RequestParam(name = "nombre",required = true) String nombre,
            @RequestParam(name = "contrasenya", required = true)String contrasenya,
            Model model
    ){
        String comprovaDades = loginService.comprovaDades(nombre, contrasenya, model);
        
        
        return comprovaDades;
        
    }
    
    
}
